<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrizeUser extends Model
{
    protected $table = 'prize_users';
}
