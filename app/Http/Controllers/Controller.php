<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use DB;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
  {
    //Share variable to all views and controllers.
    $setting_values = DB::table('settings')
    ->select('name','value')
    ->get();
    
    $settings;

    foreach($setting_values as $s){
        $settings[$s->name] = $s->value;
    }
    // Sharing is caring
    View::share('settings', $settings);
  }

  public static function send_post_notification($type,$post_id){

    $user_from = Auth::User()->id;

    $user_to = DB::table('users')
    ->join('user_posts','user_posts.author','=','users.id')
    ->where('user_posts.id','=',$post_id)
    ->select('users.id')
    ->get();

      DB::table('notifications')->insert(
        ['user_id' => $user_to[0]->id, 'post_id' => $post_id, 'user_from' => $user_from, 'type' => $type]
    );
  }

  public static function send_message_notification($user_to){

    $user_from = Auth::User()->id;

      DB::table('notifications')->insert(
        ['user_id' => $user_to,'user_from' => $user_from, 'type' => "message"]
    );
  }

  public static function send_friends_notification($user_to){

    $user_from = Auth::User()->id;

      DB::table('notifications')->insert(
        ['user_id' => $user_to,'user_from' => $user_from, 'type' => "friends"]
    );
  }

 /*  public function send_notification_entity($type,$entity_from,$user_to){
      DB::table('notifications')->insert(
        ['user_id' => $user_to, 'entity_from_id' => $user_from, 'type' => $type]
    );
  } */

}

