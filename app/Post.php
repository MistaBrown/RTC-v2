<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/* This implements model for Forum-Posts */

class Post extends Model
{
    protected $table = 'forum_posts';
    protected $fillable = ['id','body','title','archived', 'user_id' , 'image', 'category_id'];
}
