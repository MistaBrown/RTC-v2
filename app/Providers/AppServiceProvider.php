<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use DB;
use Auth;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Blade::directive('svg', function($arguments) {
            // Funky madness to accept multiple arguments into the directive
            list($path, $class) = array_pad(explode(',', trim($arguments, "() ")), 2, '');
            $path = trim($path, "' ");
            $class = trim($class, "' ");
    
            // Create the dom document as per the other answers
            $svg = new \DOMDocument();
            $svg->load(public_path($path));
            $svg->documentElement->setAttribute("class", $class);
            $output = $svg->saveXML($svg->documentElement);
    
            return $output;
        });

        View::composer(('*'), function ($view) {
            
        //Share friend informations
        if(Auth::User()){
            
            /* Passing information to all views */
            $uid = Auth::User()->id;
            
            $friends = DB::select("SELECT * FROM `friends_relations` JOIN users ON users.id = friends_relations.user_id WHERE friend_id = $uid AND user_id IN (SELECT friend_id FROM friends_relations WHERE user_id = $uid)");

            $view->with('friends', $friends);
            

            $notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, (SELECT COUNT(id)FROM notifications WHERE user_id = 1 AND hasRead != 1 AND (notifications.type = 'comment' OR notifications.type = 'like')) AS notifications_amount
            FROM (notifications JOIN users ON notifications.user_from = users.id)
            WHERE user_id = $uid AND hasRead != 1
            ORDER BY notifications.id DESC");

            $view->with('notification',$notification);

            $message_notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, (SELECT COUNT(id)FROM notifications WHERE user_id = $uid AND hasRead != 1 AND notifications.type = 'message') AS notifications_amount
            FROM (notifications JOIN users ON notifications.user_from = users.id)
            WHERE user_id = $uid AND notifications.hasRead != 1 AND notifications.type = 'message'
            ORDER BY notifications.id DESC");
            
            $view->with('message_notification',$message_notification);

            $friends_notification = DB::select("SELECT users.name as user_from_name, users.surname as user_from_surname,notifications.user_from as user_from_id,users.image,notifications.post_id, notifications.type, 
            (SELECT COUNT(id)FROM notifications WHERE user_id = $uid AND hasRead != 1 AND notifications.type = 'friends') AS notifications_amount
            FROM (notifications JOIN users ON notifications.user_from = users.id)
            WHERE user_id = $uid AND notifications.hasRead != 1 AND notifications.type = 'friends'
            ORDER BY notifications.id DESC");
            
            $view->with('friends_notification',$friends_notification);
      
            /* SUGGESTED FRIENDS */
            $suggested_friends = DB::select("SELECT users.id, users.name, users.surname, users.image FROM users ORDER BY RAND() LIMIT 5");

            $view->with('suggested_friends',$suggested_friends);

            /* FRIEND REQUESTS */
            $friends_requests = DB::select("SELECT *,COUNT(*) AS request_number FROM `friends_relations` JOIN users ON users.id = friends_relations.user_id WHERE friend_id = $uid AND user_id NOT IN (SELECT friend_id FROM friends_relations WHERE user_id = $uid)");

            $view->with('friends_requests',$friends_requests);

        }
        
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
