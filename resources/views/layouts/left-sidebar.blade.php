<aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12" style="z-index:1">
        <div class="your-profile" style="max-width: 219px;position:fixed;margin-left: 70px;">	
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="card" style="background-color: transparent;">

                        <ul class="left-menu">
                            
                            <li style="border-bottom: solid #9e9fbf 1px;">
                                <a href="/profile/user/{{Auth::User()->id}}" style="padding:16px 16px 9px 22px;">
                                @if (Auth::user()->image)
                                        <img src="{{ asset('storage/' . Auth::user()->image) }}" class="olymp-newsfeed-icon left-menu-icon avatar" style="width: 30px;height: 30px;border-radius: 50%;margin-right: 19px;" alt="author">
                                    @else
                                        <img src="img/author-page.jpg" class="olymp-newsfeed-icon left-menu-icon avatar" style="width: 25px;height: 25px;border-radius: 50%;" alt="author">
                                    @endif

                                    <span class="left-menu-title username">{{Auth::User()->name.' '.Auth::User()->surname}}</span>
                                </a>
                            </li>
                            <li>
                                <a href="/">
                                    <svg class="olymp-newsfeed-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="NEWSFEED"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-newsfeed-icon"></use></svg>
                                    <span class="left-menu-title">Feed</span>
                                </a>
                            </li>
                            <li>
                                <a href="/profile/user/{{Auth::User()->id}}">
                                    <svg class="olymp-badge-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FRIEND GROUPS"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-badge-icon"></use></svg>
                                    <span class="left-menu-title">Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="/profile/friends/user/{{Auth::User()->id}}">
                                    <svg class="olymp-happy-faces-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FRIEND GROUPS"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-faces-icon"></use></svg>
                                    <span class="left-menu-title">My Friends</span>
                                </a>
                            </li>
                            <li>
                                <a href="/find/friends">
                                    <svg class="olymp-plus-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FRIEND GROUPS"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-plus-icon"></use></svg>
                                    <span class="left-menu-title">Find Friends</span>
                                </a>
                            </li>
                            <li>
                                <a href="/profile/settings/personal-informations">
                                    <svg class="olymp-settings-v2-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="Friends Birthdays"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-settings-v2-icon"></use></svg>
                                    <span class="left-menu-title">Profile Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="/forum">
                                    <svg class="olymp-thunder-icon left-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="FRIEND GROUPS"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-thunder-icon"></use></svg>
                                    <span class="left-menu-title">Help</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>	
            </div>

    </aside>