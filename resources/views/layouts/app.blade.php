<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.head')
</head>

<body>

    @include('layouts.header')
    @include('layouts.chat')
    
    @yield('content')
    
    @include('layouts.footer')

</body>

</html>