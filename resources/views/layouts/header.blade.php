<!-- <script>
	$('body').click(function( event ) {
  event.stopPropagation();
});
</script> -->


<!-- Header-BP -->

<header class="header" id="site-header">
	<a style="text-decoration:none;cursor:pointer" class="logo">
				<div class="img-wrap">
					<img class="logo-img" onclick='window.location.href ="/"' src="{{URL::asset('images/logo.png')}}" alt="RealCommunity">
				</div>
	</a>

<div class="header-content-wrapper">
	<form class="search-bar w-search notification-list friend-requests" method="get" action="{{route('search.community')}}">
		<div class="form-group with-button">
			<input name="keyword" class="form-control js-user-search" placeholder="Search here people or pages..." type="text">
			<button>
			@svg('svg-icons/sprites/icons.svg','olymp-magnifying-glass-icon')
				<svg class="olymp-magnifying-glass-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-magnifying-glass-icon"></use></svg>
			</button>
		</div>
	</form>

	<a href="/find/friends"" class="link-find-friend">Find Friends</a>

	<div class="control-block">

		<div class="control-icon more has-items">
			<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use></svg>
			
			@if(count($friends_notification)>0) 
				@if($friends_notification[0]->notifications_amount != 0)
				<div id="friends-notification-counter" class="label-avatar bg-blue">
					{{$friends_notification[0]->notifications_amount}}
				</div>
				@else
				<div id="friends-notification-counter" style="display:none" class="label-avatar bg-blue"></div>
				@endif
			@else
				<div id="friends-notification-counter" style="display:none;" class="label-avatar bg-blue"></div>
			@endif

			<div class="more-dropdown more-with-triangle triangle-top-center">
				<div class="ui-block-title ui-block-title-small">
					<h6 class="title">FRIEND REQUESTS</h6>
					<a class="read-all-this" nt_type="friends" href="#">Mark all as read</a>
				</div>

				<div class="mCustomScrollbar" data-mcs-theme="dark">
					<ul id="friends-notification-list" class="notification-list friend-requests">
					@include('notifications.friends-notifications-block')
					</ul>
				</div>

				<a href="#" class="view-all bg-blue">Check all your Events</a>
			</div>
		</div>

		<div class="control-icon more has-items">
			<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-chat---messages-icon"></use></svg>
			@if(count($message_notification)>0)
				@if($message_notification[0]->notifications_amount != 0)
				<div id="message-notification-counter" class="label-avatar bg-primary">{{$message_notification[0]->notifications_amount}}</div>
				@else
				<div id="message-notification-counter" style="display:none" class="label-avatar bg-primary"></div>
				@endif
			@else
			<div id="message-notification-counter" style="display:none" class="label-avatar bg-primary"></div>
			@endif

			<div class="more-dropdown more-with-triangle triangle-top-center">
				<div class="ui-block-title ui-block-title-small">
					<h6 class="title">Chat / Messages</h6>
					<a class="read-all-this" nt_type="messages" href="#">Mark all as read</a>
				</div>

				<div class="mCustomScrollbar" data-mcs-theme="dark">
					<ul id="message-notification-list" class="notification-list chat-message">
						@include('notifications.message-notifications-block')
					</ul>
				</div>

				<a href="/profile/messages" class="view-all bg-purple">View All Messages</a>
			</div>
		</div>

		<div class="control-icon more has-items">
			<svg class="olymp-thunder-icon mark-read-notifications"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-thunder-icon"></use></svg>
			@if(count($notification)>0)
				@if($notification[0]->notifications_amount != 0)
				<div id="notification-counter" class="label-avatar bg-primary">{{$notification[0]->notifications_amount}}</div>
				@else
				<div id="notification-counter" style="display:none" class="label-avatar bg-primary"></div>
				@endif
			@else
			<div id="notification-counter" style="display:none" class="label-avatar bg-primary"></div>
			@endif

			<div class="more-dropdown more-with-triangle triangle-top-center">
				<div class="ui-block-title ui-block-title-small">
					<h6 class="title">Notifications</h6>
					<a class="read-all-this" nt_type="thunder" href="#">Mark all as read</a>
				</div>

				<div class="mCustomScrollbar" data-mcs-theme="dark">
					<ul id="header-notification-list" class="notification-list">
						@include('notifications.notification-block')
					</ul>
				</div>

				<a href="#" class="view-all bg-primary">View All Notifications</a>
			</div>
		</div>

		<div class="author-page author vcard inline-items more">
			<div class="author-thumb">
						@if (Auth::user()->image)
							<img src="{{ asset('storage/' . Auth::user()->image) }}" class="avatar" style="width: 24px; height:24px;" alt="author">
						@else
							<img src="{{asset('img/author-page.jpg')}}" class="avatar" alt="author">
						@endif
				<span class="icon-status online"></span>
				<div class="more-dropdown more-with-triangle">
					<div class="mCustomScrollbar" data-mcs-theme="dark">
						<div class="ui-block-title ui-block-title-small">
							<h6 class="title">Your Account</h6>
						</div>

						<ul class="account-settings">
							<li>
								<a href="/profile/user/{{Auth::User()->id}}">

									<svg class="olymp-badge-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-badge-icon"></use></svg>

									<span>Profile</span>
								</a>
							</li>
							<li>
								<a href="{{route('profile.informations')}}">

									<svg class="olymp-menu-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-menu-icon"></use></svg>

									<span>Profile Settings</span>
								</a>
							</li>
							@if(App\Http\Middleware\UserGroupCheck::check(['AdminDash']))
							<li style="border-top:1px solid #b1b8e4">
								<a href="/admin">

									<svg class="olymp-settings-v2-icon left-menu-icon" ><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-settings-v2-icon"></use></svg>

									<span>Admin Dashboard</span>
								</a>
							</li>
							@endif
							<li>
								<a href="{{route('logout')}}">
									<svg class="olymp-logout-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-logout-icon"></use></svg>

									<span>Log Out</span>
								</a>
							</li>
						</ul>
					</div>

				</div>
			</div>
			<a href="{{url('/profile/user/'.Auth::User()->id)}}" class="author-name fn">
				<div class="author-title">
					{{Auth::User()->name.' '.Auth::User()->surname}} <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-dropdown-arrow-icon"></use></svg>
				</div>
			</a>
		</div>

	</div>
</div>

</header>

<!-- ... end Header-BP -->


<!-- Responsive Header-BP -->

<header class="header header-responsive" id="site-header-responsive">

<div class="header-content-wrapper">
	<ul class="nav nav-tabs mobile-app-tabs" role="tablist">
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#request" role="tab">
				<div class="control-icon has-items">
					<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use></svg>
					<div class="label-avatar bg-blue">6</div>
				</div>
			</a>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#chat" role="tab">
				<div class="control-icon has-items">
					<svg class="olymp-chat---messages-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-chat---messages-icon"></use></svg>
					<div class="label-avatar bg-purple">2</div>
				</div>
			</a>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#notification" role="tab">
				<div class="control-icon has-items">
					<svg class="olymp-thunder-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-thunder-icon"></use></svg>
					<div class="label-avatar bg-primary">8</div>
				</div>
			</a>
		</li>

		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#search" role="tab">
				<svg class="olymp-magnifying-glass-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-magnifying-glass-icon"></use></svg>
				<svg class="olymp-close-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-close-icon"></use></svg>
			</a>
		</li>
	</ul>
</div>

<!-- Tab panes -->
<div class="tab-content tab-content-responsive">

	<div class="tab-pane " id="request" role="tabpanel">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">FRIEND REQUESTS</h6>
				<a href="/find/friends">Find Friends</a>
				<a href="#">Settings</a>
			</div>
			<ul class="notification-list friend-requests">
				
			</ul>
			<a href="#" class="view-all bg-blue">Check all your Events</a>
		</div>

	</div>

	<div class="tab-pane " id="chat" role="tabpanel">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">Chat / Messages</h6>
				<a href="#">Mark all as read</a>
				<a href="#">Settings</a>
			</div>

			<ul class="notification-list chat-message">
			
			</ul>

			<a href="#" class="view-all bg-purple">View All Messages</a>
		</div>

	</div>

	<div class="tab-pane " id="notification" role="tabpanel">

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">Notifications</h6>
				<a href="#">Mark all as read</a>
				<a href="#">Settings</a>
			</div>

			<ul class="notification-list">
				<li>
					<div class="author-thumb">
						<img src="{{asset('img/avatar62-sm.jpg')}}" alt="author">
					</div>
					<div class="notification-event">
						<div><a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>
						<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">4 hours ago</time></span>
					</div>
									<span class="notification-icon">
										<svg class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
									</span>

					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
					</div>
				</li>

				<li class="un-read">
					<div class="author-thumb">
						<img src="{{asset('img/avatar63-sm.jpg')}}" alt="author">
					</div>
					<div class="notification-event">
						<div>You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just became friends. Write on <a href="#" class="notification-link">his wall</a>.</div>
						<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">9 hours ago</time></span>
					</div>
									<span class="notification-icon">
										<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use></svg>
									</span>

					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
					</div>
				</li>

				<li class="with-comment-photo">
					<div class="author-thumb">
						<img src="{{asset('img/avatar64-sm.jpg')}}" alt="author">
					</div>
					<div class="notification-event">
						<div><a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your <a href="#" class="notification-link">photo</a>.</div>
						<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>
					</div>
									<span class="notification-icon">
										<svg class="olymp-comments-post-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use></svg>
									</span>

					<div class="comment-photo">
						<img src="{{asset('img/comment-photo1.jpg')}}" alt="photo">
						<span>“She looks incredible in that outfit! We should see each...”</span>
					</div>

					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
					</div>
				</li>

				<li>
					<div class="author-thumb">
						<img src="{{asset('img/avatar65-sm.jpg')}}" alt="author">
					</div>
					<div class="notification-event">
						<div><a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to attend to his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.</div>
						<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>
					</div>
									<span class="notification-icon">
										<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use></svg>
									</span>

					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
					</div>
				</li>

				<li>
					<div class="author-thumb">
						<img src="{{asset('img/avatar66-sm.jpg')}}" alt="author">
					</div>
					<div class="notification-event">
						<div><a href="#" class="h6 notification-friend">James Summers</a> commented on your new <a href="#" class="notification-link">profile status</a>.</div>
						<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>
					</div>
									<span class="notification-icon">
										<svg class="olymp-heart-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use></svg>
									</span>

					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-little-delete"></use></svg>
					</div>
				</li>
			</ul>

			<a href="#" class="view-all bg-primary">View All Notifications</a>
		</div>

	</div>

	<div class="tab-pane " id="search" role="tabpanel">


			<form class="search-bar w-search notification-list friend-requests" method="get" action="{{route('search.community')}}">
				<div class="form-group with-button">
					<input name="keyword" class="form-control js-user-search" placeholder="Search here people or pages..." type="text">
				</div>
			</form>


	</div>

</div>
<!-- ... end  Tab panes -->

</header>

<!-- ... end Responsive Header-BP -->


<form id="logout-form" action="{{ route('logout') }}" method="POST" hidden>
    @csrf
</form>

<script>
    function logout(){
        $('#logout-form').submit();
	}
	
	function searchMessage() {
		alert('Nothing found');
	}
</script>