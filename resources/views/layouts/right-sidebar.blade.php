<!-- Right Sidebar -->
<aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12" style="z-index: 1;max-height: 688px">
		<div>
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Friend Suggestions</h6>
					<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use></svg></a>
				</div>
	
				<!-- W-Action -->
					<ul class="widget w-friend-pages-added notification-list friend-requests">
					@foreach($suggested_friends as $sf)
						<li class="inline-items">
							<div class="author-thumb">
								<img src="{{asset('storage/' . $sf->image)}}" alt="author">
							</div>
							<div class="notification-event">
								<a href="/profile/user/{{$sf->id}}" class="h6 notification-friend">{{$sf->name." ".$sf->surname}}</a>
								<span class="chat-message-item">8 Friends in Common</span>
							</div>
							<span class="notification-icon">
								<a href="#" class="accept-request send-friend-request-short" friend_id="{{$sf->id}}">
									<span class="icon-add without-text">
										<svg class="olymp-happy-face-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-happy-face-icon"></use></svg>
									</span>
								</a>
							</span>
						</li>
					@endforeach
					</ul>
				
				<!-- ... end W-Action -->
			</div>
				
				<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Pages you may like</h6>
				</div>

				<ul class="widget w-friend-pages-added notification-list friend-requests">
					<li class="inline-items">
						<div class="author-thumb">
							<img src="{{asset('img/avatar38-sm.jpg')}}" alt="author">
						</div>
						<div class="notification-event">
							<a href="#" class="h6 notification-friend">The Marina Bar</a>
							<span class="chat-message-item">Restaurant / Bar</span>
						</div>
						<span class="notification-icon" data-toggle="tooltip" data-placement="top" data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								<svg class="olymp-star-icon"><use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-star-icon"></use></svg>
							</a>
						</span>
				
					</li>
				</ul>
			</div>

		</div>
		</aside>

		<!-- ... end Right Sidebar -->