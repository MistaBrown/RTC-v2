@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('news.store') }}" style="margin-top: 12%">
  {{ csrf_field() }}
  <div class="bootstrap-iso">
    <div class="container-fluid">
     <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
       <form method="post">
        <div class="form-group ">
      
         <input class="form-control" id="title" name="title" placeholder="Insert Title" type="text"/>
        </div>
        <div class="form-group ">

         <textarea class="form-control" cols="40" id="content" name="content" rows="10"></textarea>
        </div>
        <div class="form-group">
         <div>
          <button class="btn btn-primary " name="submit" type="submit">
           Inserisci
          </button>
         </div>
        </div>
       </form>
      </div>
     </div>
    </div>
   </div>

@endsection