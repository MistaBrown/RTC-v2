@extends('admin.dash-layout')
@section('content')
<div class="row">
            <div class="text-center">
                <h2>Gestione News</h2>
            </div>
            
            <table class="table table-fixed table-striped jambo_table ">
                    <thead style="padding-right:18px;">
                        <tr>
                            <th class="col" style="border-bottom:0px!important;width:5%;">Id</th>
                            <th class="col" style="border-bottom:0px!important;width:25%;">Titolo Post</th>
                            <th class="col" style="border-bottom:0px!important;width:10%;">Contenuto</th>
                        </tr>
                    </thead>
                    <tbody>
                      
                        @if(count($news)>0)
                            
                        @foreach ($news as $s)
                                <tr style="cursor: pointer;">
                                    <td class="col" style="width:5%;" id="news_id">{{$s->id}}</td>
                                    <td class="col" style="width:25%;" id="news_title">{{$s->title}}</td>
                                    <td class="col" style="width:10%;" id="news_content">{{$s->content}}</td>
                                    <td class="col" style="width:20%;display:table;" ><a class="btn btn-success btn-sm enable edit" style="margin:2px 5px 2px 5px">Modifica</a></td>
                                </tr>
                            @endforeach
                        @else
                                <tr>
                                    <th> Ancora Nessuna News!</th>
                                </tr>
                        @endif
    
                        
                    </tbody>
                </table>
            </div>
        <div class="row">
            <form method="POST" id="news_form" action="{{ route('news.insertOrUpdate') }}" style="margin-top: 12%" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="bootstrap-iso">
                <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <form method="post">
                    <div class="form-group ">
                        <input type="hidden" id="form_news_id" name="news_id"/>
                    <input class="form-control" id="form_title" name="title" placeholder="Insert Title" type="text" required />
                    </div>
                    <div class="form-group ">
                    <textarea class="form-control" cols="40" id="form_content" name="content" rows="10" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Image to upload</label><br>
                        <input type="file"  name="image" id="image" class='image'>
                    </div>

                    </div>
                    <button class="btn btn-primary " style="display:block;" name="submit" type="submit">Inserisci</button>
                    <button class="btn btn-primary" id="cancel" style="display:block;" type="button">Annulla</button>
                    </div>
                    </div>
                    </form>
                    </div>
                </div>
                </div>
                </div>
            </form>
        </div>
    

          <style>
                table {
                    width: 50%;
                }
                
                thead, tbody, tr, td, th {
                    display: block;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    content: ' ';
                    display: block;
                    visibility: hidden;
                }
            </style>
            
    <script>

        jQuery(document).ready(function() {
            $(".edit").click(function() {
                var news_id = $(this).parent().parent().find("#news_id").text();
                var title = $(this).parent().parent().find("#news_title").text();
                var content = $(this).parent().parent().find("#news_content").text();

                $("#form_news_id").val(news_id);
                $("#form_title").val(title);
                $("#form_content").val(content);      
            });

            $(".cancel").click(function() {
                resetForm();
            });
        });

        function resetForm(){
            $("#form_news_id").val("");
            $("#form_title").val("");
            $("#form_content").val("");
        }

    </script>
    
    
@endsection