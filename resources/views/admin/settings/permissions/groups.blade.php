@extends('admin.dash-layout')
@section('content')

<div class="greyscreen"></div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="container">

            <div class="row">
                <h2>Gestione Gruppi</h2>
            </div>


            <table class="table table-fixed table-striped jambo_table">
                <thead style="padding-right:18px;">
                    <tr>
                        <th class="col"
                            style="border-bottom:0px!important;width:10%;">Id</th>
                        <th class="col"
                            style="border-bottom:0px!important;width:35%;">Nome</th>
                        <th class="col"
                            style="border-bottom:0px!important;width:35%;">Descrizione</th>
                        <th class="col"
                            style="border-bottom:0px!important;width:10%;"></th>
                        <th class="col"
                            style="border-bottom:0px!important;width:10%;"></th>
                    </tr>
                </thead>
                <tbody>

                    @if(count($groups)>0)

                    @foreach ($groups as $s)
                    <tr style="cursor: pointer;">
                        <td class="col" style="width:10%;" id="groupId">{{$s->id}}</th>
                        <td class="col" style="width:35%;" id="name">{{$s->name}}</td>
                        <td class="col" style="width:35%;" id="description">{{$s->description}}</td>
                        @if($s->id == 1)
                        <td class="col"
                            style="width:10%;display:table;vertical-align:center"></td>
                        <td class="col" style="width:10%;display:table;"></td>
                        @else
                        <td class="col"
                            style="width:10%;display:table;vertical-align:center"><a
                                class="btn btn-warning btn-disabled btn-sm edit"
                                style="margin:2px 5px 2px 5px">Modifica</a></td>
                        <td class="col" style="width:10%;display:table;"><a
                                class="btn btn-danger btn-sm delete"
                                style="margin:2px 5px 2px 5px">Elimina</a></td>
                        @endif
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <th> Ancora Nessuna Gruppo!</th>
                    </tr>
                    @endif


                </tbody>
            </table>
        </div>


    </div>
    <div class="col-md-1"></div>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci un Gruppo <small></small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" method="POST"
                        action="{{route('settings.manageGroups')}}">
                        {{ csrf_field() }}
                        <div class="form-group">

                            <label class="control-label col-md-3 col-sm-3
                                col-xs-12">Nome Gruppo</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input required name="groupName"
                                    id="form_description" class="form-control"
                                    placeholder="Nome Gruppo" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3
                                col-xs-12">Descrizione Gruppo</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input required name="groupDesc"
                                    id="form_description" class="form-control"
                                    placeholder="Descrizione Gruppo"
                                    type="text">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-9 col-sm-9 col-xs-12
                                col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Annulla</button>
                                <button type="submit" class="btn btn-success">Invia</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<div id="edit-popup" class="popup-div" style="display:none;">
    <div class="x_panel tile fixed_height_320 overflow_hidden">
        <div class="x_title">
            <h2 id="#edit-title">Modifica Gruppo</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li style="left: 80%;">
                    <a id="close-popup" class="">
                        <i class="fa fa-close"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
                <form class="form-horizontal form-label-left" method="POST" action="/admin/settings/permissions/groups">
                    {{ method_field('PUT') }}
                    <input name="_token" value="7YCF8Zi0ZaFijFmgMV81OglEIwAKVdMn66BPKwEN" type="hidden">
                    <div class="form-group">

                        <label class="control-label col-md-3 col-sm-3
                            col-xs-12">Nome Gruppo</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input required="" name="groupName" id="edit-group-name" class="form-control" placeholder="Nome Gruppo" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3
                            col-xs-12">Descrizione Gruppo</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input required="" name="groupDescription" id="edit-group-description" class="form-control" placeholder="Descrizione Gruppo" type="text">
                            <input hidden type="text" id="edit-group-groupId" name="groupId">
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12
                            col-md-offset-3">
                            <button type="reset" class="btn btn-primary">Annulla</button>
                            <button type="submit" class="btn btn-success">Invia</button>
                        </div>
                    </div>

                </form>
        </div>
        <!-- end X-content -->
    </div>
</div>


<style>
                table {
                    width: 50%;
                }
                
                thead, tbody, tr, td, th {
                    display: block;
                }
                
                thead th {
                    height: 30px;
                }
                
                tbody {
                    overflow: -moz-scrollbars-vertical;
                    overflow-y: scroll;
                    height: 273px;
                }
                
                tbody td, thead th {
                    float: left;
                    width: 20%;
                }
                
                tr:after {
                    clear: both;
                    content: ' ';
                    display: block;
                    visibility: hidden;
                }
</style>

<script>

        jQuery(document).ready(function() {
        
        $("#close-popup").click(function(){
            $('#edit-popup').hide();
            $('.greyscreen').hide();
        })

        $(".greyscreen").click(function(){
            $('#edit-popup').hide();
            $(this).hide();
        })

        $(".edit").click(function() {
            
            $('#edit-popup').show();
            $('.greyscreen').show();
            
            var groupName = $(this).parent().parent().find("#name").text()
            var groupDesc = $(this).parent().parent().find("#description").text()
            var groupId = $(this).parent().parent().find("#groupId").text()

            $("#edit-group-name").val(groupName);
            $("#edit-group-description").val(groupDesc);
            $("#edit-group-groupId").val(groupId);

            console.log('id='+groupId);


            });
        });

        jQuery(document).ready(function() {
          $(".delete").click(function() {
            var group_id = $(this).parent().parent().find("#groupId").text();
            
            if(confirm("Vuoi davvero eliminare il gruppo: "+($(this).parent().parent().find("#name").text())+" ?"))
              $.ajax({
                url : "/admin/settings/permissions/groups/"+group_id,
                type : "DELETE",
                data: {
                   
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : false,
                success: function(response) {
                  location.reload();
                },
                error: function(response) {
                   console.log('errore delete');
                }
            });
            else
            return false;
          });
        });
</script>


@endsection