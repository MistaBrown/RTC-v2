@extends('profile.settings.settings-layout')
@section('settings-content')
<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
    <div class="ui-block">
        <div class="ui-block-title">
            <h6 class="title">Friend Requests</h6>
        </div>

        <ul class="notification-list friend-requests">
            @if(isset($friends_requests[0]->id))
            @foreach($friends_requests as $fr)
                <li>
                    <div class="author-thumb">
                        <img src="{{asset('storage/').'/'.$fr->image}}" alt="author">
                    </div>
                    <div class="notification-event">
                        <a href="{{url('/profile/user/'.$fr->id)}}" class="h6 notification-friend">{{$fr->name." ".$fr->surname}}</a>
                    </div>
                    <span class="notification-icon">
                        <div id="accept-friend-request" friend_id="{{$fr->id}}" style="cursor:pointer" class="accept-request">
                            <span class="icon-add">
                                <svg class="olymp-happy-face-icon">
                                    <use xlink:href="icons/icons.svg#olymp-happy-face-icon"></use>
                                </svg>
                            </span>
                            Accept Friend Request
                        </div>
                    </span>
                </li>
            @endforeach
            @endif
        </ul>
    </div>
</div>
@endsection