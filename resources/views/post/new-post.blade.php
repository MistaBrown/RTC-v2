<div id="post-{{$post->pid}}" class="ui-block">
                        <!-- Post -->
                        
                        <article class="hentry post">
                        
                            <div class="post__author author vcard inline-items">
                                <img src="{{ asset('storage/').'/'.Auth::User()->image}}" alt="author">
                        
                                <div class="author-date">
                                    <a class="h6 post__author-name fn">{{Auth::User()->name.' '.Auth::User()->surname}}</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            {{$post->created_at}}
                                        </time>
                                    </div>
                                </div>
                        
                                <div class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
                                    </svg>
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Cancella Post</a>
                                        </li>
                                    </ul>
                                </div>
                        
                            </div>
                        
                            <p style="font-size:19px">{{$post->body}}</p>
                            @if(!empty($post->image))
                            <center><img class="post-image-wd" src="{{asset('/storage/'.$post->image)}}"></center>
                            @endif

                            <div class="post-additional-info inline-items">
                        
                                <a class="post-add-icon inline-items">
                                    <svg class="olymp-heart-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
                                    </svg>
                                    <span>{{$post->likes}}</span>
                                </a>
                        
                                <ul class="friends-harmonic">
                            @foreach($feed_likes as $fl)
                                @if($fl->lpid == $post->pid)
                                    <li>
                                        <a href="#">
                                            <img src="{{ asset('storage/').'/'.$post->user_image}}" alt="friend">
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                                </ul>
                        
                                <div class="names-people-likes">
                                    @if($post->likes > 2)
                                    <a href="#">You</a>, <a href="#">Elaine</a>
                                    and
                                    <br>34 more liked this
                                    @elseif($post->likes == 1)
                                    <a href="#">You</a>
                                    <br>liked this
                                    @elseif($post->likes == 0)
                                    <strong>Noone</strong>
                                    <br>liked this
                                    @else
                                    <a href="#">You</a> and <a href="#">Elaine</a>
                                    <br>liked this
                                    @endif
                                </div>
                        
                        
                                <div class="comments-shared">
                                    <a onclick="$('#comment-list-{{$post->pid}}').toggle()" class="post-add-icon inline-items">
                                        <svg class="olymp-speech-balloon-icon">
                                            <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-speech-balloon-icon"></use>
                                        </svg>
                                        <span>17</span>
                                    </a>
                                </div>
                        
                        
                            </div>
                        
                            <div class="control-block-button post-control-button">
                        
                                <a href="#" class="btn btn-control">
                                    <svg class="olymp-like-post-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-like-post-icon"></use>
                                    </svg>
                                </a>
                        
                                <a href="#" class="btn btn-control">
                                    <svg class="olymp-comments-post-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use>
                                    </svg>
                                </a>

                        
                            </div>
                        
                        </article>