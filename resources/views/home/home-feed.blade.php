               <div id="newsfeed-items-grid">
                   
                   @if($surveys)
               			<div class="ui-block grey-bg feed">
                            <ul class="survey-block notification-list chat-message chat-message-field">		
                                <div class="row">
                                    <li>
                                        <div class="col-lg-2 col-mg-2 col-sm-2 col-xs-12" style="float:left">
                                            <div class="author-thumb">
                                            @if (Auth::user()->image)
                                                <img src="{{ asset('storage/' . Auth::user()->image) }}" class="avatar" style="width: 48px; height:48px;" alt="author">
                                            @else
                                                <img src="img/author-page.jpg" alt="author">
                                            @endif
                                                <span class="survey-author composition-name">{{ Auth::user()->name }}</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-10 col-mg-10 col-sm-10 col-xs-12">
                                            <div class="notification-event">
                                                <a href="#" class="h6 notification-friend">{{ $surveys[0]->title }}</a>
                                                <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:30pm</time></span>
                                                <span class="chat-message-item" style="width:100%">{{ $surveys[0]->description }}</span>
                                                <div class="added-photos">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="birthday-item badges col-lg-12 col-mg-12 col-sm-12 col-xs-12" style="text-align:center">
                                                <div class="skills-item">
                                                    <div class="skills-item-meter">
                                                        <span class="skills-item-meter-active skills-animate" style="width: 100%; opacity: 1;"></span>
                                                        <span class="survey-author composition-name" style="font-size:9pt">Risposte</span>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="col-lg-12 col-mg-12 col-sm-12 col-xs-12">
                                        <form method="POST" action="{{route('survey.storeHome')}}">
                                            {{ csrf_field() }}
                                            @foreach($survey_options as $key=>$n)
                                                <div class="birthday-item inline-items badges">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input name="survey_option_id" value="{{ $n->id }}" type="checkbox"><span class="checkbox-material"></span>
                                                        </label>
                                                    </div>
                                                    <div class="birthday-author-name">
                                                        <a href="#" class="h6 author-name">Opzione numero {{++$key}}</a>
                                                        <div class="birthday-date">{{$n->description}}</div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <input name="survey_id" type="hidden" value="{{ $surveys[0]->id }}">
                                        </div>
                                        <button style="float: right; margin-right: 10px;" type="submit" class="btn btn-primary btn-md-2">Vote<div class="ripple-container"></div></button>
                                        </form>
                                    </li>
                                </div>
                            </ul>
                        </div>
                    @endif

                    <!-- post-feed -->

                    @if ($feed)
					@foreach ($feed as $n)
					
					
                    <div id="post-{{$n->pid}}" class="ui-block">
                        <!-- Post -->
                        
                        <article class="hentry post">
                        
                            <div class="post__author author vcard inline-items">
                                <img src="{{ asset('storage/').'/'.$n->user_image}}" alt="author">
                        
                                <div class="author-date">
                                    <a class="h6 post__author-name fn">{{$n->author}}</a>
                                    <div class="post__date">
                                        <time class="published" datetime="2017-03-24T18:18">
                                            {{$n->created}}
                                        </time>
                                    </div>
                                </div>
                        
                                <div class="more">
                                    <svg class="olymp-three-dots-icon">
                                        <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
                                    </svg>
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Cancella Post</a>
                                        </li>
                                    </ul>
                                </div>
                        
                            </div>
                        
                            <p style="font-size:19px">{{$n->body}}</p>
                            @if(!empty($n->image))
                            <div><img class="post-image-wd" src="{{asset('/storage/'.$n->image)}}"></div>
                            @endif

                            <div id="bottom-bar-{{$n->pid}}" class="bottom-bar">
                                <div class="post-additional-info inline-items">
                            
                                    <a id="like-bottom-{{$n->pid}}" post-id="{{$n->pid}}" class="like-post post-add-icon inline-items">
                                        @if($n->has_liked == '1')
                                        <svg class="svgClicked olymp-heart-icon">
                                        @else
                                        <svg class="olymp-heart-icon">
                                        @endif
                                            <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
                                        </svg>
                                        <span>{{$n->likes}}</span>
                                    </a>
                            
                                    <ul class="friends-harmonic">
                                @foreach($feed_likes as $fl)
                                    @if($fl->lpid == $n->pid)
                                        <li>
                                            <a href="#">
                                                <img src="{{ asset('storage/').'/'.$fl->image}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$fl->name}}" alt="friend">
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                                    </ul>
                            
                                    <div class="names-people-likes">
                                        @if($n->likes > 2 && $n->has_liked==1)
                                        <a href="#">You</a>
                                        </a>
                                        and
                                        <br>{{$n->likes}} more liked this 
                                        @elseif($n->likes == 1 && $n->has_liked==1)
                                        <a href="#">You</a>
                                        <br>liked this 
                                        @elseif($n->likes == 1 && !$n->has_liked==1 )
                                        1 liked this 
                                        @elseif($n->likes > 1 && !$n->has_liked==1 )
                                        {{$n->likes}} liked this   
                                        @elseif($n->likes == 0)
                                        <strong>Noone</strong>
                                        <br>liked this
                                        @else
                                        <a href="#">You</a> and</a>
                                        <br>{{$n->likes - 1}} more liked this 
                                        @endif
                                    </div>
                            
                            
                                    <div class="comments-shared">
                                        <a onclick="$('#comment-list-{{$n->pid}}').toggle()" class="post-add-icon inline-items">
                                            <svg class="olymp-speech-balloon-icon">
                                                <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-speech-balloon-icon"></use>
                                            </svg>
                                            <span>17</span>
                                        </a>
                                    </div>

                            
                        </div>
                    </div><!-- end bottom bar container -->
                    
                    <div class="control-block-button post-control-button">
                        
                        @if($n->has_liked == '1')
                        <a id="like-top-{{$n->pid}}" post-id="{{$n->pid}}" class="iconClicked like-post btn btn-control">
                            @else
                            <a id="like-top-{{$n->pid}}" post-id="{{$n->pid}}" class="like-post btn btn-control">
                                @endif
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-like-post-icon"></use>
                                </svg>
                            </a>
                            
                            <a post-id="{{$n->pid}}" class="btn btn-control">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-comments-post-icon"></use>
                                </svg>
                            </a>
                            
                            
                        </div>
                        
                    </article>
                    
                    <!-- .. end Post -->					
                    <!-- Comment Block -->
                    <ul id="comment-list-{{$n->pid}}" style="display:none" class="comments-list">
						<li class="comment-item">
							<div class="post__author author vcard inline-items">
								<img src="img/avatar10-sm.jpg" alt="author">
					
								<div class="author-date">
									<a class="h6 post__author-name fn" href="#">Elaine Dreyfuss</a>
									<div class="post__date">
										<time class="published" datetime="2017-03-24T18:18">
											5 mins ago
										</time>
									</div>
								</div>
					
								<a href="#" class="more">
									<svg class="olymp-three-dots-icon">
										<use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-three-dots-icon"></use>
									</svg>
								</a>
					
							</div>
					
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.</p>
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon">
									<use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-heart-icon"></use>
								</svg>
								<span>8</span>
							</a>
							<a href="#" class="reply">Reply</a>
						</li>
                        <!-- End Comment Block -->
                        <div style="background-color:white">    
                            <a href="#" class="more-comments">View more comments <span>+</span></a>
                            <form class="comment-form inline-items">
                            
                                <div class="post__author author vcard inline-items">
                                    <img src="img/author-page.jpg" alt="author">
                            
                                    <div class="form-group with-icon-right is-empty">
                                        <textarea class="form-control" placeholder=""></textarea>
                                        <div class="add-options-message">
                                            <a href="#" class="options-message">
                                                <svg class="olymp-camera-icon">
                                                    <use xlink:href="{{asset('svg-icons/sprites/icons.svg')}}#olymp-camera-icon"></use>
                                                </svg>
                                            </a>
                                        </div>
                                    <span class="material-input"></span></div>
                                </div>
                            
                                <button class="btn btn-md-2 btn-primary">Post Comment</button>
                            
                                <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>
                        
                            </form>
                        </div>
					</ul>
                    
                        <!-- ... end Comment Form  -->				
                    </div>

					
					@endforeach
    				@endif