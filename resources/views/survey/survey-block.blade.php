@section('survey-block')

<style>
    .table th{
        text-align:center;
    }
</style>

    @if(count($surveys)>0)
    
    <table class="table table-striped">
        <tbody>
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Created at</th>
                </tr>
            </thead>
        @foreach ($surveys as $s)
            <tr class='clickable-row' data-href='survey/{{$s->id}}' style="cursor: pointer;">
                <th scope="row">{{$s->id}}</th>
                <td>{{$s->title}}</td>
                <td>{{$s->description}}</td>
                <td>{{$s->created_at}}</td>
            </tr>
        @endforeach
    @else
            <tr>
                <th> Ancora Nessun Survey!</th>
            </tr>
    @endif
        </tbody>
    </table>

    <script>
    jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
    </script>
@endsection