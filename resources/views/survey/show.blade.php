@extends('layouts.app')

@section('content')
@if ($surveys)
@if ($survey)
<h2>{{ $survey->title }} </h2>
<form method="POST" action="{{ route('survey.store') }}">
{{ csrf_field() }}
<input name="survey_id" value="{{ $survey->id }}" style="display:none;" id="survey_id">  
@foreach ($surveys as $n)
    <div> 
        <input name="survey_option_id" value="{{ $n->survey_options_id }}" style="display:inline;" id="survey_option" type="radio">  
        <label>{{ $n->survey_options_descr }} </label>
    </div>
@endforeach
<button type="submit" action="" class="btn btn-default">Submit</button>
</form>

@endif
@endif

@endsection