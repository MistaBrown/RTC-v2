@extends('layouts.app')

@section('content')
<div class="container" style="background-color: #fff6">

    @if ($liveEvents)
        @foreach ($liveEvents as $n)
            <ul>   
                <li>{{ $n->title }} </li>
                <li>{{ $n->url }} </li>
         </ul>
        @endforeach
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
