@extends('layouts.app')

@section('content')
@if ($liveEvent)

<h2>{{$liveEvent->title}}</h2>

<p>{{$liveEvent->url}}</p>

@endif

@endsection