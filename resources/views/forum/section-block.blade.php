@section('section-block')
<!-- Categories Block -->
@if ($sections)
    @foreach ($sections as $sec)
    <table class="forums-table">
				
            <thead>
        
            <tr>
        
                <th class="forum">
                    {{$sec->description}}
                </th>
        
                <th class="topics">
                    Topic
                </th>
        
                <th class="posts">
                    Post
                </th>
        
                <th class="freshness">
                    Ultima Attività
                </th>
        
            </tr>
        
            </thead>
        
            <tbody>
        
                @foreach ($categories as $cat)
                    @if($cat->section == $sec->id )
                    <tr>
                            <td class="forum">
                                <div class="forum-item">
                                    <img src="/img/forum2.png" alt="forum">
                                    <div class="content">
                                        <a href="/forum/category/{{$cat->id}}" class="h6 title">{{$cat->description}}</a>
                                        <p class="text">Descrizione Categoria</p>
                                    </div>
                                </div>
                            </td>
                            <td class="topics">
                                <a href="#" class="h6 count">NUMERO TOPIC CATEGORIA</a>
                            </td>
                            <td class="posts">
                                <a href="#" class="h6 count">NUMERO POST CATEGORIA</a>
                            </td>
                            <td class="freshness">
                                <div class="author-freshness">
                                    <div class="author-thumb">
                                        <img src="/img/avatar51-sm.jpg" alt="author">
                                    </div>
                                    <a href="#" class="h6 title">Nicholas Grissom</a>
                                    <time class="entry-date updated" datetime="2017-06-24T18:18">2 hours, 7 minutes ago</time>
                                </div>
                            </td>
                        </tr>
                    @endif
                 @endforeach
            </ul>
    @endforeach
@endif

</div>
</div>
@endsection