@extends('layouts.app')
@section('content')


<!-- ... end Responsive Header-BP -->
<div class="header-spacer header-spacer-small"></div>


<!-- Main Header Groups -->

<div class="main-header spacer">
	<div class="content-bg-wrap bg-group"></div>
	<div class="container">
		<div class="row">
			<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
				<div class="main-header-content">
					<h1>Welcome to the Forums!</h1>
					<p>Here in the forums you’ll be able to easily create and manage categories and topics to share with the
	 community! We included some of the most used topics, like music, comics, movies, and community, each one with a cool
	  and customizable illustration so you can have fun with them! </p>
				</div>
			</div>
		</div>
	</div>

	
</div>

<!-- ... end Main Header Groups -->
<div class="container">
	<div class="row">
		<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			
		</div>
	</div>
</div>



<div class="container">
	<div class="row">
		<div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12" style="margin:0 auto";>

			<div class="ui-block">
                <div class="ui-block-title bg-blue">
                        <h6 class="title c-white">Create a post</h6>
                    </div>
                    <div class="ui-block-content">
                        <form method="post" action="/forum/create">
                            {{csrf_field()}}
                            <div class="row">
    
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    
                                    <div class="form-group is-empty">
                                        <label class="control-label">Title</label>
                                        <input class="form-control" placeholder="" name="title" required type="text">
                                    <span class="material-input"></span></div>

                                    <div class="btn-group bootstrap-select form-control open">
                                            <label class="control-label">Category</label>
                                            <select class="selectpicker form-control" tabindex="-98" name="category_id" required>
                                                @foreach($categories as $c)
                                                <option value="{{$c->id}}">{{$c->description}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <label class="control-label">Text</label>
                                    <div class="form-group label-floating">
                                        <label class="control-label">Text</label>
                                        <textarea id="reply_body" class="form-control summernote"  style="height: 240px" name="body"></textarea>
                                    <span class="material-input"></span></div>
    
                                </div>
    
                                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <button type="reset" onclick="cleanSummer()" class="btn btn-secondary btn-lg full-width">Cancel</button>
                                </div>
    
                                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <button type="submit" class="btn btn-blue btn-lg full-width">Send Post</button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        
        </div>

	</div>
</div>

<a class="back-to-top" href="#">
	<img src="/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>


@endsection
