@extends('layouts.app')
@include('forum.post-block')
@section('content')


<!-- ... end Responsive Header-BP -->
<div class="header-spacer header-spacer-small"></div>


<!-- Main Header Groups -->

<div class="main-header spacer">
	<div class="content-bg-wrap bg-group"></div>
	<div class="container">
		<div class="row">
			<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
				<div class="main-header-content">
					<h1>Welcome to the Forums!</h1>
					<p>Here in the forums you’ll be able to easily create and manage categories and topics to share with the
	 community! We included some of the most used topics, like music, comics, movies, and community, each one with a cool
	  and customizable illustration so you can have fun with them! </p>
				</div>
			</div>
		</div>
	</div>

	
</div>

<!-- ... end Main Header Groups -->
<div class="container">
	<div class="row">
		<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="ui-block responsive-flex">
				<div class="ui-block-title">
					<div class="h6 title"><a class="h6 title" href="{{route('forum.index')}}"> RTC Forum </a> / {{$path['section']}} /  <a class="h6 title" href="/forum/category/{{$path['category_id']}}">{{$path['category']}}</a></div>
					<form class="w-search">
						<div class="form-group with-button">
							<input class="form-control" type="text" placeholder="Cerca nel forum...">
							<button>
								<svg class="olymp-magnifying-glass-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use></svg>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">

			<div class="ui-block">

				<table class="forums-table">

				@yield('post-block')
				<!-- Forums Table -->

				</table>

				<!-- ... end Forums Table -->
			</div>

		</div>

		<div class="col col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Featured Topics</h6>
				</div>
				<div class="ui-block-content">


					<!-- Widget Featured Topics -->

					<ul class="widget w-featured-topics">
						<li>
							<i class="icon fa fa-star" aria-hidden="true"></i>
							<div class="content">
								<a href="#" class="h6 title">The new Goddess of War trailer was launched at E3!</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">2 hours, 16 minutes ago</time>
							</div>
						</li>
						<li>
							<i class="icon fa fa-star" aria-hidden="true"></i>
							<div class="content">
								<a href="#" class="h6 title">This year’s ComixCon will have the best presentations</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">14 hours, 36 minutes ago</time>
							</div>
						</li>
						<li>
							<i class="icon fa fa-star" aria-hidden="true"></i>
							<div class="content">
								<a href="#" class="h6 title">Here are the behind-the-scenes photos of “Vilords”</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">9 hours, 8 minutes ago</time>
							</div>
						</li>
					</ul>

					<!-- ... end Widget Featured Topics -->
				</div>
			</div>

			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Recent Topics</h6>
				</div>
				<div class="ui-block-content">


					<!-- Widget Recent Topics -->

					<ul class="widget w-featured-topics">
						<li>
							<div class="content">
								<a href="#" class="h6 title">Summer is Coming! Picnic in the east boulevard park</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">26 minutes ago</time>
								<div class="forums">The Community</div>
							</div>
						</li>
						<li>
							<div class="content">
								<a href="#" class="h6 title">Kung Fighters released a new video, check it out here!</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">44 minutes ago</time>
								<div class="forums">The Boombox</div>
							</div>
						</li>
						<li>
							<div class="content">
								<a href="#" class="h6 title">What’s your favourite season?</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">59 minutes ago</time>
								<div class="forums">The Community</div>
							</div>
						</li>
						<li>
							<div class="content">
								<a href="#" class="h6 title">Who had the best presentation at this year’s E3? Rate them!</a>
								<time class="entry-date updated" datetime="2017-06-24T18:18">1 hour, 3 minutes ago</time>
								<div class="forums">Arcade Planet</div>
							</div>
						</li>
					</ul>

					<!-- ... end Widget Recent Topics -->
				</div>
			</div>
		</div>
	</div>
</div>

<a class="back-to-top" href="#">
	<img src="/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>


@endsection
