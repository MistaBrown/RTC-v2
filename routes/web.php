<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function() {

    // Login Routes...
        Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
        Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
        Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    
    // Registration Routes...
        Route::get('register', ['as' => 'register', 'uses' => 'Auth\LoginController@showLoginForm']); //redirect to login/registration page
        Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
    
    // Password Reset Routes...
        Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
        Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
        Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
        Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

    });

Route::post('/news/like', 'NewsController@like');
Route::post('/settings', 'HomeController@update')->name('settings.update');
Route::post('survey/vote', 'SurveyController@storeHome')->name('survey.storeHome');
Route::post('/news/comment', 'NewsController@comment')->name('news.news_comment');
Route::get('/news/comment', 'NewsController@getComment')->name('news.get_news_comment');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


/* Admin panel routes */

Route::group(['middleware' => ['auth']], function() {
    
    Route::post('/notification/all', 'notificationController@allNotificationCheck');
    
    Route::get('/', 'HomeController@index')->name('home');
    
    /** Testing Area **/
    Route::get('/test/createUsers', 'Test\databaseSeeder@createUsers');
    Route::get('/test/addFriends', 'Test\databaseSeeder@addFriends');
    Route::post('/notification/read', 'notificationController@read');

    
    /* User Account zone */
    Route::get('/profile/user/{uid}','HomeController@showProfile');
    Route::get('/profile/friends/user/{uid}','HomeController@showFriends');
    Route::get('/profile/settings/notifications', 'HomeController@notifications')->name('profile.notifications');
    Route::get('/profile/settings/personal-informations', 'HomeController@personal_informations')->name('profile.informations');
    Route::get('/profile/settings/friend-requests', 'HomeController@friend_requests')->name('profile.friend_requests');
    Route::get('/profile/settings/change-password', 'HomeController@changePassword')->name('profile.change-password');
    Route::post('/profile/settings/change-password', 'HomeController@updatePassword')->name('profile.change-password');
    Route::get('/profile/messages', 'HomeController@profileMessenger')->name('profile.messages');
    Route::post('/profile/settings/change/user-photo', 'HomeController@changeUserPhoto');
    Route::post('/profile/settings/update/info', 'HomeController@updateInfo')->name('update.userInfo');
    Route::post('/profile/settings/accept/request/{friend}', 'HomeController@acceptFriendship')->name('accept.friend');
    Route::post('/profile/settings/send/request/{friend}', 'HomeController@requestFriendship')->name('accept.friend');
    Route::post('/profile/messenger/get/conversation/{friend}', 'HomeController@getConversation')->name('profile.messenger.get-conversation');
    Route::get('/find/friends','HomeController@findFriends');
    
    
    /* Chat zone */
    Route::post('/message/new/get/{friend_id}', 'chatController@message_new_get');
    Route::post('/chat/user/{uid}', 'chatController@getChat')->name('chat.getChat');
    Route::post('/chat/message/user/{uid}', 'chatController@sendMessage')->name('chat.sendMessage');
    Route::post('/profile/messenger/send/message/{friend}', 'chatController@sendMessage_ProfileMessenger')->name('profile.messenger.send');
    
    Route::get('/search/community','HomeController@search')->name('search.community');
    
    /* Post zone */
    Route::post('/post/like','HomeController@likePost');
    Route::delete('/post/like','HomeController@dislikePost');
    Route::post('/comment/post','HomeController@commentPost');
    Route::post('/create/post', 'HomeController@insertPost')->name('insertPost');
    Route::post('/create/post/preloadImage', 'HomeController@imagePreload');
    Route::post('/status/post','HomeController@postStatus')->name('post.status');
    Route::get('/feed/load-more/{page}', 'HomeController@loadMore');
    
    
    /* Forum zone */
    Route::post('/forum/showpost/{pid}', 'PostController@insertEditReply')->name('forum.insertEditReply');
    Route::get('/forum/create', 'ForumController@showCreate')->name('post.showCreate');
    Route::post('/forum/create', 'ForumController@createPost')->name('category.createPost');
    
    /*------ ADMIN ZONE ------ */
    Route::get('/admin','AdminController@index');

    Route::get('/admin/prize', 'PrizeController@manage')->name('prize.manage');
    Route::post('/admin/prize/insert', 'PrizeController@insertOrUpdate')->name('prize.insertOrUpdate');

    Route::get('/admin/liveEvent', 'LiveEventController@manage')->name('liveEvent.manage');
    Route::post('/admin/liveEvent/insert', 'LiveEventController@insertOrUpdate')->name('liveEvent.insertOrUpdate');

    Route::get('/admin/news', 'NewsController@manage')->name('news.manage');
    Route::post('/admin/news/insert', 'NewsController@insertOrUpdate')->name('news.insertOrUpdate');

    Route::get('admin/survey/create', 'SurveyController@create')->name('survey.adminCreate');
    Route::get('/admin/survey/create/options', 'SurveyController@options')->name('survey.options');
    Route::post('/admin/survey/create', 'SurveyController@insertOrUpdate')->name('survey.insertOrUpdate');

    Route::get('/admin/forum/category', 'ForumCategoryController@manage')->name('category.manage');
    Route::post('/admin/forum/category', 'ForumCategoryController@insertOrUpdate')->name('category.insertOrUpdate');
    Route::get('/admin/forum/category/archived', 'ForumCategoryController@archived')->name('category.archived');
    
    Route::get('/admin/forum/section', 'SectionController@manage')->name('section.manage');
    Route::post('/admin/forum/section', 'SectionController@insertOrUpdate')->name('section.insertOrUpdate');
    Route::get('/admin/forum/section/archived', 'SectionController@archived')->name('section.archived');
    
    Route::get('/admin/forum/post', 'PostController@manage')->name('post.manage');
    Route::post('/admin/forum/post', 'PostController@insertOrUpdate')->name('post.insertOrUpdate');
    Route::get('/admin/forum/post/archived', 'PostController@archived')->name('post.archived');
 
    Route::get('/admin/users', 'UsersController@manage')->name('user.manage');
    Route::post('/admin/users', 'UsersController@insertOrUpdate')->name('user.insertOrUpdate');
    Route::get('/admin/users/archived', 'UsersController@archived')->name('user.archived');
    Route::get('/admin/users/groups', 'UsersController@manageUserGroups')->name('user.groups');
    Route::post('/admin/users/groups', 'UsersController@updateUserGroups');
    Route::get('/admin/user/roleGroup/{$id}','UsersController@getRoleGroup');
    Route::get('/admin/users/friends', 'UsersController@manage')->name('user.friends');

    Route::get('/admin/settings/content', 'SettingsController@show')->name('settings.content');
    Route::post('/admin/settings/content', 'SettingsController@edit')->name('settings.content');

    /* Admin - Manage groups and permissions */
    Route::get('/admin/settings/permissions', 'PermissionController@show')->name('settings.permissions');
    Route::post('/admin/settings/permissions', 'PermissionController@editPermissions')->name('settings.permissions');
    Route::post('/admin/settings/permissions/init', 'PermissionController@init');
    Route::get('/admin/settings/permissions/groups', 'PermissionController@showGroups')->name('settings.manageGroups');
    Route::post('/admin/settings/permissions/groups', 'PermissionController@addGroup')->name('settings.manageGroups');
    Route::put('/admin/settings/permissions/groups', 'PermissionController@editGroup')->name('settings.editGroups');
    Route::delete('/admin/settings/permissions/groups/{groupId}', 'PermissionController@removeGroup')->name('settings.removeGroups');


});    

Route::resource('survey', 'SurveyController');
Route::resource('news', 'NewsController');
Route::resource('liveEvent','LiveEventController');
Route::resource('prize', 'PrizeController');
    

/* End Admin Panel routes */



Route::get('/search', 'ForumController@search')->name('forum.search');
Route::get('/forum', 'ForumController@index')->name('forum.index');
Route::get('/forum/category/{cat}', 'ForumCategoryController@show')->name('forum.showCategory');
Route::get('/forum/showpost/{pid}', 'PostController@show')->name('forum.showPost');



/*
Route::get('/news/insert', function() {
    return view('news/insert');
});

Route::post('/news/insert', function() {
    return view('news/insert');
});
*/